import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Article, NewsResponse } from '../interfaces';
import { Observable, of } from 'rxjs';
import { map} from 'rxjs/operators';
import { ArticlesByCategoryAndPage } from '../interfaces/index';

const apiKey = environment.apiKey;
const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  private articlesByCategory : ArticlesByCategoryAndPage = {};

  constructor(private http : HttpClient) { }

  private executeQuery<T>(endpoint : string){
    console.log('Peticion realizada');
    return this.http.get<T>(`${apiUrl}${endpoint}`,{
      params: {
        apiKey: apiKey,
        country: 'us',
      }
    })
  }

  getTopHeadLines(): Observable<Article[]>{

    return this.getTopHeadlinesByCategory('business');
    //return this.http.get<NewsResponse>(`https://newsapi.org/v2/top-headlines?country=us&category=business`,{
    //  params: {
    //    apiKey
    //  }
    //}).pipe(
    //  map( ({articles}) => articles)
    //);
  }

  getTopHeadlinesByCategory( category : string, loadMore: boolean = false ): Observable<Article[]>{

    if(loadMore){
      return this.getArticlesByCategory(category);
    }

    if(this.articlesByCategory[category]){
      return of(this.articlesByCategory[category].articles);
    }

    return this.getArticlesByCategory(category);
  }

  private getArticlesByCategory( category : string): Observable<Article[]> {

    if(Object.keys(this.articlesByCategory).includes(category)){

    }else{
      this.articlesByCategory[category] = {
        page: 0,
        articles: []
      }
    }

    const page = this.articlesByCategory[category].page + 1;

    return this.executeQuery<NewsResponse>(`/top-headlines?category=${ category }&page=${ page }`).pipe(
      map( ({articles }) => {

        if(articles.length === 0) return this.articlesByCategory[category].articles;

        this.articlesByCategory[category] = {
          page: page,
          articles: [...this.articlesByCategory[category].articles, ...articles]
        }

        return this.articlesByCategory[category].articles;
      })
    );

  }
}
