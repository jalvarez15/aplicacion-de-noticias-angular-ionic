import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetButton, ActionSheetController, Platform } from '@ionic/angular';

import { Article } from '../../interfaces/index';

import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

import { StorageService } from '../../services/storage.service';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent {

  @Input() article: Article;
  @Input() index: number;

  constructor(
    private iab: InAppBrowser,
    private platform: Platform,
    private actionSheetController: ActionSheetController,
    private socialSharing: SocialSharing,
    private storageService: StorageService
  ) { }

  async onOpenMenu() {

    const articleInFavorite = this.storageService.articleInFavorite(this.article);

    const btns: ActionSheetButton[] = [
      {
        text: articleInFavorite ? 'Remover de favoritos':'Favorito',
        icon: articleInFavorite ? 'heart':'heart-outline',
        handler: () => this.onToggleFavorite()
      },
      {
        text: 'Cancelar',
        icon: 'close-outline',
        role: 'cancel'
      }
    ];

    const share: ActionSheetButton = {
      text: 'Compartir',
      icon: 'share-outline',
      handler: () => this.onShareArticle()
    }

    if(this.platform.is('capacitor')){
      btns.unshift(share);
    }

    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      buttons: btns
    });


    await actionSheet.present();

  }

  onShareArticle() {

    const { title, source, url } = this.article;
    this.socialSharing.share(
      title,
      source.name,
      null,
      url
    );

  }

  onToggleFavorite() {
    this.storageService.saveRemoveArticle(this.article);
  }

  abrirArticulo() {
    if (this.platform.is('ios') || this.platform.is('android')) {
      const browser = this.iab.create('https://ionicframework.com/');
      browser.show();
      return;
    }
    window.open(this.article.url, '_blanck')

  }
}
